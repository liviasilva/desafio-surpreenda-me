  $(document).ready(function(){
  //Botões de Login e Cadastro
    $('.modal').modal();
//Quero ajudar/Empresa
  $('ul.tabs').tabs('select_tab','cadastroPessoa');
//Imagens dos projetos
  $('.materialboxed').materialbox();
  $(".button-collapse").sideNav();

  //Animação do título
  $(".animetext").hide();
  $(".animetext").show(2000);

  });


//Tags para pesquisa por categoria
$('.chips').material_chip(); 
  $('.chips-initial').material_chip({
    data: [{
      tag: 'Educação',
    }, {
      tag: 'Moradia',
    }, {
      tag: 'Construção',
    }],
  });
  $('.chips-placeholder').material_chip({
    placeholder: 'Enter a tag',
    secondaryPlaceholder: '+Tag',
  });
  $('.chips-autocomplete').material_chip({
    autocompleteOptions: {
      data: {
        'Educação': null,
        'Moradia': null,
        'Construção': null
      },
      limit: Infinity,
      minLength: 1
    }
  });



//plugin paginação
      $('.pagination').materializePagination({
          align: 'center',
          lastPage:  10,
          firstPage:  1,
          urlParameter: 'page',
          useUrlParameter: true,
          onClickCallback: function(requestedPage){
              console.log('Requested page is '+ requestedPage);
          }
      });