package com.maosaobra.maosaobra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaosaobraApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaosaobraApplication.class, args);
	}
}
